defmodule InstrumentStoreWeb.Router do
  use InstrumentStoreWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {InstrumentStoreWeb.LayoutView, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", InstrumentStoreWeb do
    pipe_through(:browser)

    get("/", GuitarController, :index)
    resources("/guitars", GuitarController)
  end
end
